
---------------REGIONS

DELIMITER |
DROP PROCEDURE IF EXISTS dataforet.insert_region |
CREATE PROCEDURE insert_region(IN regionName VARCHAR(255))
BEGIN
    IF NOT EXISTS(SELECT idRegion FROM region WHERE nomRegion=regionName)
    THEN
        INSERT INTO region VALUES (regionName);
    END IF;
END |

---------------DEPARTEMENTS

DELIMITER |
DROP PROCEDURE IF EXISTS dataforet.insert_departement |
CREATE PROCEDURE insert_departement(IN departmentNumber VARCHAR(255), IN departmentName VARCHAR(255), IN regionName VARCHAR(255))
BEGIN
    DECLARE region_id INT DEFAULT 1;
    IF NOT EXISTS(SELECT idDepartement FROM departement WHERE nomDepartement= departmentName AND numDepartement=departmentNumber)
    THEN
        SET region_id = (SELECT idRegion FROM region WHERE nomRegion=regionName);
        INSERT INTO departement VALUES (idDepartement, departmentName, departmentNumber, region_id);
    END IF;
END |

---------------COMMUNES

DELIMITER |
DROP PROCEDURE IF EXISTS dataforet.insert_commune |
CREATE PROCEDURE insert_commune(IN inseeCode VARCHAR(255), IN townName VARCHAR(255), IN epciCode VARCHAR(255))
BEGIN
    DECLARE epci_id INT DEFAULT 1;
    IF NOT EXISTS(SELECT idCommune FROM commune WHERE codeINSEE=inseeCode AND nomCommune = townName)
    THEN
        SET epci_id = (SELECT idEpci FROM epci WHERE codeEPCI=epciCode);
        INSERT INTO commune VALUES (idCommune, inseeCode, townName, epci_id);
    END IF;
END;

---------------EPCI

DELIMITER |
DROP PROCEDURE IF EXISTS dataforet.insert_epci |
CREATE PROCEDURE insert_epci(IN epciCode VARCHAR(255), IN epciName VARCHAR(255), IN departmentNum varchar(255))
BEGIN
    DECLARE department_id INT DEFAULT 0;
    IF NOT EXISTS(SELECT idEpci FROM epci WHERE codeEPCI=epciCode)
    THEN
        SET department_id = (SELECT idDepartement FROM departement WHERE numDepartement=departmentNum);
        INSERT INTO epci VALUES (idEpci, epciCode, epciName, department_id);
    END IF;
END;

---------------FORET

DELIMITER |
DROP PROCEDURE IF EXISTS dataforet.insert_foret |
CREATE PROCEDURE insert_foret(IN forestRegNumber VARCHAR(255), IN forestName VARCHAR(255), IN forestGPSCoos VARCHAR(255), IN isDomaniale INT(1), IN inseeTownCode VARCHAR(255))
BEGIN
    DECLARE town_id INT DEFAULT 1;
    IF NOT EXISTS(SELECT idForet FROM foret WHERE matriculeForet=forestRegNumber)
    THEN
        SET town_id = (SELECT idCommune FROM commune WHERE codeINSEE=inseeTownCode);
        INSERT INTO foret VALUES (idForet, forestRegNumber, forestName, forestGPSCoos, isDomaniale, town_id);
    END IF;
END;



