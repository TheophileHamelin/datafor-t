# -*- coding: utf8 -*-
import datetime
import sys
from datetime import datetime

import csv
import mysql.connector


def displayHelp():
    print(" ______        ____   ,---------.    ____     ________     ,-----.    .-------.        .-''-. ,---------.\n"
          "|    _ `''.  .'  __ `.\          \ .'  __ `. |        |  .'  .-,  '.  |  _ _   \     .'_ _   \\           \\\n"
          "| _ | ) _  \/   '  \  \`--.  ,---'/   '  \  \|   .----' / ,-.|  \ _ \ | ( ' )  |    / ( ` )   '`--.  ,---'\n"
          "|( ''_'  ) ||___|  /  |   |   \   |___|  /  ||  _|____ ;  \  '_ /  | :|(_ o _) /   . (_ o _)  |   |   \\\n"
          "| . (_) `. |   _.-`   |   :_ _:      _.-`   ||_( )_   ||  _`,/ \ _/  || (_,_).' __ |  (_,_)___|   :_ _:\n"
          "|(_    ._) '.'   _    |   (_I_)   .'   _    |(_ o._)__|: (  '\_/ \   ;|  |\ \  |  |'  \   .---.   (_I_)\n"
          "|  (_.\.' / |  _( )_  |  (_(=)_)  |  _( )_  ||(_,_)     \ `\"/  \  ) / |  | \ `'   / \  `-'    /  (_(=)_)\n"
          "|       .'  \ (_ o _) /   (_I_)   \ (_ o _) /|   |       '. \_/``\".'  |  |  \    /   \       /    (_I_)\n"
          "'-----'`     '.(_,_).'    '---'    '.(_,_).' '---'         '-----'    ''-'   `'-'     `'-..-'     '---'  ")

    print("This project is called DataForet. The goal is to fill a database called dataforet\n"
          "Use :\n"
          "python3 script.py -> This will run the script to populate the database called dataforet\n"
          "/!\This script doesn't not create the database/!\ \n")


def log(text):
    """
    This method allow the script to log an event in "log.xt" file
    :param text: The text to log
    """
    log_file = open("error.txt", 'a')
    log_file.write("{0} -- {1}\n".format(datetime.now().strftime("%Y-%m-%d %H:%M"), text))
    log_file.close()


def populate_region_table(csv_file):
    """
    Function used to populate the table "region" from a CSV file
    :param csv_file: Name of the CSV file that contains regions data
    """
    try:
        with open(csv_file, encoding='utf-8') as csv_file:
            # Reading CSV file
            reader = csv.reader(csv_file, delimiter=';', quotechar='"')
            # Skipping headers
            next(reader)

            for regionRow in reader:
                # args must be a list
                if not regionRow:
                    # Nothing is inserted to preserve the integrity of the database
                    raise ValueError("Region : Empty cells at the line " + str(reader.line_num))
                else:
                    args = (regionRow[0],)

                # Check about regions already in the table is done in stored procedure 'insert_region'
                cursor.callproc('insert_region', args)

            print("\033[92mPopulation de la table 'region' effectuée.\033[0m")

    except IOError as e:
        log(format(e))
        print("\033[91mImpossible d'ouvrir le fichier " + csv_file + ", annulation des opérations\033[0m")
        # Rollback
        database.rollback()
        # Database deco.
        print("\033[92mDéconnexion de la base de données\033[0m")
        cursor.close()
        database.close()
        # End of the script to preserve database integrity
        sys.exit(-1)
    except ValueError as e2:
        log(format(e2))
        # Rollback
        database.rollback()
        print("\033[91mLe nom d'une région ne peut être vide\033[0m")
        # Database deco.
        print("\033[92mDéconnexion de la base de données\033[0m")
        cursor.close()
        database.close()
        # End of the script to preserve database integrity
        sys.exit(-1)


def populate_departement_table(csv_file):
    """
    Function used to populate the table "departement" from a CSV file
    :param csv_file: Name of the CSV file that contains departments data
    """
    try:
        with open(csv_file, encoding='utf-8') as csv_file:
            # Reading CSV file
            reader = csv.reader(csv_file, delimiter=';', quotechar='"')
            # Skipping headers
            next(reader)
            for departementRow in reader:
                # args must be a list
                # Test if cells are empty
                if not departementRow[0] or not departementRow[1] or not departementRow[2]:
                    # Nothing is inserted to preserve the integrity of the database
                    raise ValueError("Departement : Empty cells at the line " + str(reader.line_num))
                else:
                    args = (departementRow[0], departementRow[1], departementRow[2])

                # Check about departments already in the table is done in stored procedure 'insert_departement'
                cursor.callproc('insert_departement', args)

            print("\033[92mPopulation de la table 'departement' effectuée.\033[0m")

    except IOError as e:
        log(format(e))
        print("\033[91mImpossible d'ouvrir le fichier " + csv_file + ", annulation des opérations\033[0m")
        # Rollback
        database.rollback()
        # Database deco.
        print("\033[92mDéconnexion de la base de données\033[0m")
        cursor.close()
        database.close()
        # End of the script to preserve database integrity
        sys.exit(-1)
    except ValueError as e2:
        log(format(e2))
        # Rollback
        database.rollback()
        print("\033[91mLes attributs d'un département ne peuvent être vides\033[0m")
        # Database deco.
        print("\033[92mDéconnexion de la base de données\033[0m")
        cursor.close()
        database.close()
        # End of the script to preserve database integrity
        sys.exit(-1)


def populate_commune_table(csv_file):
    """
    Function used to populate the table "commune" from a CSV file
    :param csv_file: Name of the CSV file that contains towns data
    """
    try:
        with open(csv_file, encoding='utf-8') as csv_file:
            # Reading CSV file
            reader = csv.reader(csv_file, delimiter=';', quotechar='"')
            # Skipping headers
            next(reader)
            for communeRow in reader:
                # args must be a list
                # Test if cells are empty
                if not communeRow[0] or not communeRow[1] or not communeRow[2]:
                    # Nothing is inserted to preserve the integrity of the database
                    raise ValueError("Commune : Empty cells at the line " + str(reader.line_num))
                else:
                    args = (communeRow[0], communeRow[1], communeRow[2])

                # Check about departments already in the table is done in stored procedure 'insert_departement'
                cursor.callproc('insert_commune', args)

            print("\033[92mPopulation de la table 'commune' effectuée.\033[0m")

    except IOError as e:
        log(format(e))
        print("\033[91mImpossible d'ouvrir le fichier " + csv_file + ", annulation des opérations\033[0m")
        # Rollback
        database.rollback()
        # Database deco.
        print("\033[92mDéconnexion de la base de données\033[0m")
        cursor.close()
        database.close()
        # End of the script to preserve database integrity
        sys.exit(-1)
    except ValueError as e2:
        log(format(e2))
        # Rollback
        database.rollback()
        print("\033[91mLes attributs d'une commune ne peuvent être vides\033[0m")
        # Database deco.
        print("\033[92mDéconnexion de la base de données\033[0m")
        cursor.close()
        database.close()
        # End of the script to preserve database integrity
        sys.exit(-1)


def populate_epci_table(csv_file):
    """
    Function used to populate the table "epci" from a CSV file
    :param csv_file: Name of the CSV file that contains epci's data
    """
    try:
        with open(csv_file, encoding='utf-8') as csv_file:
            # Reading CSV file
            reader = csv.reader(csv_file, delimiter=';', quotechar='"')
            # Skipping headers
            next(reader)
            for epciRow in reader:
                # args must be a list
                # Test if cells are empty
                if not epciRow[0] or not epciRow[1] or not epciRow[2]:
                    # Nothing is inserted to preserve the integrity of the database
                    raise ValueError("EPCI : Empty cells at the line " + str(reader.line_num))
                else:
                    args = (epciRow[0], epciRow[1], epciRow[2])

                # Check about departments already in the table is done in stored procedure 'insert_departement'
                cursor.callproc('insert_epci', args)

            print("\033[92mPopulation de la table 'epci' effectuée.\033[0m")

    except IOError as e:
        log(format(e))
        print("\033[91mImpossible d'ouvrir le fichier " + csv_file + ", annulation des opérations\033[0m")
        # Rollback
        database.rollback()
        # Database deco.
        print("\033[92mDéconnexion de la base de données\033[0m")
        cursor.close()
        database.close()
        # End of the script to preserve database integrity
        sys.exit(-1)
    except ValueError as e2:
        log(format(e2))
        # Rollback
        database.rollback()
        print("\033[91mLes attributs d'un EPCI ne peuvent être vides\033[0m")
        # Database deco.
        print("\033[92mDéconnexion de la base de données\033[0m")
        cursor.close()
        database.close()
        # End of the script to preserve database integrity
        sys.exit(-1)


def populate_foret_table(csv_file):
    """
    Function used to populate the table "foret" from a CSV file
    :param csv_file: Name of the CSV file that contains forests data
    """
    try:
        with open(csv_file, encoding='utf-8') as csv_file:
            # Reading CSV file
            reader = csv.reader(csv_file, delimiter=';', quotechar='"')
            # Skipping headers
            next(reader)
            for foretRow in reader:
                # args must be a list
                # Test if cells are empty
                if not foretRow[0] or not foretRow[1] or not foretRow[2] or not foretRow[3] or not foretRow[4]:
                    # Nothing is inserted to preserve the integrity of the database
                    raise ValueError("Foret : Empty cells at the line " + str(reader.line_num))
                else:
                    # Check the field 4 : is the forest state-owned or not
                    if foretRow[3] == "Forêt non domaniale":
                        args = (foretRow[0], foretRow[1], foretRow[2], 0, foretRow[4])
                    else:
                        args = (foretRow[0], foretRow[1], foretRow[2], 1, foretRow[4])

                # Check about departments already in the table is done in stored procedure 'insert_departement'
                cursor.callproc('insert_foret', args)

            print("\033[92mPopulation de la table 'foret' effectuée.\033[0m")

    except IOError as e:
        log(format(e))
        print("\033[91mImpossible d'ouvrir le fichier " + csv_file + ", annulation des opérations\033[0m")
        # Rollback
        database.rollback()
        # Database deco.
        print("\033[92mDéconnexion de la base de données\033[0m")
        cursor.close()
        database.close()
        # End of the script to preserve database integrity
        sys.exit(-1)
    except ValueError as e2:
        log(format(e2))
        # Rollback
        database.rollback()
        print("\033[91mLes attributs d'un forêt ne peuvent être vides\033[0m")
        # Database deco.
        print("\033[92mDéconnexion de la base de données\033[0m")
        cursor.close()
        database.close()
        # End of the script to preserve database integrity
        sys.exit(-1)


# MAIN
# Database connection
if len(sys.argv) == 1:
    try:
        database = mysql.connector.connect(user='root', database='dataforet', password='')
        cursor = database.cursor()
        print("\033[92mConnexion à la base de données établie\033[0m")
        database.autocommit = False

        # Starting transaction
        database.start_transaction()

        # Populating region table...
        populate_region_table("regions.csv")

        # Populating departement table...
        populate_departement_table("departements.csv")

        # Populating epci table...
        populate_epci_table("epci.csv")

        # Populating commune table...
        populate_commune_table("communes.csv")

        # Populating foret table...
        populate_foret_table("foret.csv")

        database.commit()

        if database.is_connected():
            print("\033[92mDéconnexion de la base de données\033[0m")
            cursor.close()
            database.close()

        sys.exit(0)

    except NameError as nameError:
        print("\033[91mErreur de connexion à la base de données\033[0m")
        log(format(nameError))
        sys.exit(-1)
    except mysql.connector.Error as databaseError:
        print("\033[91mErreur de connexion à la base de données\033[0m")
        log(format(databaseError))
        sys.exit(-1)
else:
    args = sys.argv
    if args[1] == "help" or args[1] == "h":
        displayHelp()
