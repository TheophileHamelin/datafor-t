<?php


require_once PATH_MODEL . "/exceptionsTemplates/MyPDOExceptionTemplate.php";


//Exceptions
class DatabaseException extends MyPDOExceptionTemplate
{
}

class Dao
{
    private $connection;

    //DATABASE
    /* Connection to the database */
    public function __construct()
    {
        try {
            $string = "mysql:host=" . HOST . ";dbname=" . BD . ";charset=UTF8";
            $pdo_options[PDO::MYSQL_ATTR_INIT_COMMAND] = 'SET NAMES utf8';
            $this->connection = new PDO($string, LOGIN, PASSWORD, $pdo_options);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de se connecter à la base de données");
        }
    }

    public function getForests()
    {
        try {
            $stmt = $this->connection->prepare("SELECT * FROM foret LIMIT 50;");
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible d'obtenir les forêts");
        }
    }

    public function getForestById($forestId){
        try {
            $stmt = $this->connection->prepare("SELECT * FROM foret f
                                                    INNER JOIN commune c on f.idCommune = c.idCommune
                                                    INNER JOIN epci e on c.idEpci = e.idEpci
                                                    INNER JOIN departement d on e.idDepartement = d.idDepartement
                                                    INNER JOIN region r on d.idRegion = r.idRegion
                                                WHERE idForet = ?;"
            );
            $stmt->bindParam(1, $forestId);
            $stmt->execute();
            return $stmt->fetch();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible d'obtenir les forêts");
        }
    }

    public function getTownById($townId){
        try {
            $stmt = $this->connection->prepare("SELECT * FROM commune c
                                                    INNER JOIN epci e on c.idEpci = e.idEpci
                                                WHERE idCommune = ?;"
            );
            $stmt->bindParam(1, $townId);
            $stmt->execute();
            return $stmt->fetch();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible d'obtenir les communes");
        }
    }

    public function getEpciById($epciId){
        try {
            $stmt = $this->connection->prepare("SELECT * FROM epci e
                                                    INNER JOIN departement d on e.idDepartement = d.idDepartement
                                                WHERE idEpci = ?;"
            );
            $stmt->bindParam(1, $epciId);
            $stmt->execute();
            return $stmt->fetch();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible d'obtenir les epcis");
        }
    }

    public function getDepartmentById($departmentId){
        try {
            $stmt = $this->connection->prepare("SELECT * FROM departement d
                                                    INNER JOIN region r on d.idRegion = r.idRegion
                                                WHERE idDepartement = ?;"
            );
            $stmt->bindParam(1, $departmentId);
            $stmt->execute();
            return $stmt->fetch();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible d'obtenir les départements");
        }
    }

    public function getRegionById($regionId){
        try {
            $stmt = $this->connection->prepare("SELECT * FROM region r
                                                WHERE idRegion = ?;"
            );
            $stmt->bindParam(1, $regionId);
            $stmt->execute();
            return $stmt->fetch();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible d'obtenir les régions");
        }
    }

    public function getTowns(){
        try {
            $stmt = $this->connection->prepare("SELECT * FROM commune ORDER BY nomCommune;");
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible d'obtenir les communes");
        }
    }

    public function getDepartments(){
        try {
            $stmt = $this->connection->prepare("SELECT * FROM departement ORDER BY nomDepartement;");
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible d'obtenir les départements");
        }
    }

    public function getRegions(){
        try {
            $stmt = $this->connection->prepare("SELECT * FROM region ORDER BY nomRegion;");
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible d'obtenir les régions");
        }
    }

    public function getEpcis(){
        try {
            $stmt = $this->connection->prepare("SELECT * FROM epci ORDER BY nomEpci;");
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible d'obtenir les EPCI");
        }
    }

    public function updateForest($forestId, $forestName, $forestRegNumber, $gps, $stateOwned, $townId){
        try{
            $stmt = $this->connection->prepare("UPDATE foret SET
                                                nomForet = ?,
                                                matriculeForet = ?,
                                                coordonnees = ?,
                                                estDomaniale = ?,
                                                idCommune = ?
                                                WHERE idForet = ?;");
            $stmt->bindParam(1,$forestName);
            $stmt->bindParam(2,$forestRegNumber);
            $stmt->bindParam(3,$gps);
            $stmt->bindParam(4,$stateOwned);
            $stmt->bindParam(5,$townId);
            $stmt->bindParam(6,$forestId);
            $stmt->execute();
        } catch(PDOException $e){
            throw new DatabaseException("Impossible de mettre à jour la forêt");
        }
    }

    public function updateTown($townId, $insee, $townName, $epciId){
        try{
            $stmt = $this->connection->prepare("UPDATE commune SET
                                                codeINSEE = ?,
                                                nomCommune = ?,
                                                idEpci = ?
                                                WHERE idCommune = ?;");
            $stmt->bindParam(1,$insee);
            $stmt->bindParam(2,$townName);
            $stmt->bindParam(3,$epciId);
            $stmt->bindParam(4,$townId);
            $stmt->execute();
        } catch(PDOException $e){
            throw new DatabaseException("Impossible de mettre à jour la commune");
        }
    }

    public function updateEpci($epciId, $epciCode, $epciName, $departmentId){
        try{
            $stmt = $this->connection->prepare("UPDATE epci SET
                                                codeEPCI = ?,
                                                nomEpci = ?,
                                                idDepartement = ?
                                                WHERE idEpci = ?;");
            $stmt->bindParam(1,$epciCode);
            $stmt->bindParam(2,$epciName);
            $stmt->bindParam(3,$departmentId);
            $stmt->bindParam(4,$epciId);
            $stmt->execute();
        } catch(PDOException $e){
            throw new DatabaseException("Impossible de mettre à jour l'epci");
        }
    }

    public function updateDepartment($departmentId, $departmentName, $departmentNum, $regionId){
        try{
            $stmt = $this->connection->prepare("UPDATE departement SET
                                                nomDepartement = ?,
                                                numDepartement = ?,
                                                idRegion = ?
                                                WHERE idDepartement = ?;");
            $stmt->bindParam(1,$departmentName);
            $stmt->bindParam(2,$departmentNum);
            $stmt->bindParam(3,$regionId);
            $stmt->bindParam(4,$departmentId);
            $stmt->execute();
        } catch(PDOException $e){
            throw new DatabaseException("Impossible de mettre à jour le département");
        }
    }

    public function updateRegion($regionId, $regionName){
        try{
            $stmt = $this->connection->prepare("UPDATE region SET
                                                nomRegion = ?
                                                WHERE idRegion = ?;");
            $stmt->bindParam(1,$regionName);
            $stmt->bindParam(2,$regionId);
            $stmt->execute();
        } catch(PDOException $e){
            throw new DatabaseException("Impossible de mettre à jour la région");
        }
    }

    public function deleteForest($forestId){
        try {
            $stmt = $this->connection->prepare("DELETE from foret WHERE idForet=?;");
            $stmt->bindParam(1,$forestId);
            $stmt->execute();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de supprimer la forêt");
        }
    }

    public function deleteTown($townId){
        try {
            $stmt = $this->connection->prepare("DELETE from commune WHERE idCommune=?;");
            $stmt->bindParam(1,$townId);
            $stmt->execute();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de supprimer la commune");
        }
    }

    public function deleteEpci($epciId){
        try {
            $stmt = $this->connection->prepare("DELETE from epci WHERE idEpci=?;");
            $stmt->bindParam(1,$epciId);
            $stmt->execute();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de supprimer l'epci");
        }
    }

    public function deleteDepartment($departmentId){
        try {
            $stmt = $this->connection->prepare("DELETE from departement WHERE idDepartement=?;");
            $stmt->bindParam(1,$departmentId);
            $stmt->execute();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de supprimer la forêt");
        }
    }

    public function deleteRegion($regionId){
        try {
            $stmt = $this->connection->prepare("DELETE from region WHERE idRegion=?;");
            $stmt->bindParam(1,$regionId);
            $stmt->execute();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de supprimer la forêt");
        }
    }

}
