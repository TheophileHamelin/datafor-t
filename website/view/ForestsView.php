<?php


class ForestsView
{
    public function generateForestsView($forest, $towns, $epcis, $regions, $departments)
    {
        ?>
        <!DOCTYPE html>
        <html lang="fr">
        <!--        HEAD-->
        <?php require_once "view/includes/head.html" ?>

        <body>
        <?php
        require_once "view/includes/header.html";
        ?>
        <div id="mainContainer" class="white container z-depth-5">
            <div class="section">
                <h5 class="center">Consulter/Modifier</h5>
            </div>
            <div class="section">
                <?php
                //                    var_dump($forest);
                ?>
                <div class="row">
                    <form class="col s12" method="post"
                          action="index.php?updateForest=<?php echo($forest['idForet']); ?>">
                        <div class="row">
                            <div class="input-field col s6">
                                <input value="<?php echo $forest['nomForet']; ?>" id="forestName" name="forestName"
                                       type="text" class="validate">
                                <label class="active" for="first_name2">Nom</label>
                            </div>
                            <div class="input-field col s6">
                                <input value="<?php echo $forest['coordonnees']; ?>" id="gps" name="gps" type="text"
                                       class="validate">
                                <label class="active" for="first_name2">Coordonnees</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <select id="stateOwned" name="stateOwned">
                                    <?php
                                    if ($forest['estDomaniale']) {
                                        ?>
                                        <option value="1" selected>Oui</option>
                                        <option value="0">Non</option>
                                        <?php
                                    } else {
                                        ?>
                                        <option value="0" selected>Non</option>
                                        <option value="1">Oui</option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label>Commune</label>
                            </div>
                            <div class="input-field col s6">
                                <select id="town" name="town">
                                    <option value="<?php echo $forest['idCommune']; ?>"
                                            selected><?php echo $forest['nomCommune']; ?></option>
                                    <?php
                                    foreach ($towns as $town) {
                                        ?>
                                        <option
                                            value="<?php echo $town['idCommune']; ?>"><?php echo $town['nomCommune']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label>Commune</label>
                            </div>
                        </div>
                        <div class="row">
                            <!--                            <div class="input-field col s6">-->
                            <!--                                <input value="--><?php //echo $forest['codeINSEE'];
                            ?><!--" id="name" type="text" class="validate">-->
                            <!--                                <label class="active" for="first_name2">Code INSEE</label>-->
                            <!--                            </div>-->
                            <!--                            <div class="input-field col s6">-->
                            <!--                                <select>-->
                            <!--                                    <option value="--><?php //echo $forest['idEpci'];
                            ?><!--" selected>--><?php //echo $forest['nomEpci'];
                            ?><!--</option>-->
                            <!--                                    --><?php
                            //                                    foreach ($epcis as $epci){
                            //
                            ?>
                            <!--                                        <option value="--><?php //echo $epci['idEpci'];
                            ?><!--">--><?php //echo $epci['nomEpci'];
                            ?><!--</option>-->
                            <!--                                        --><?php
                            //                                    }
                            //
                            ?>
                            <!--                                </select>-->
                            <!--                                <label>EPCI</label>-->
                            <!--                            </div>-->
                            <div class="input-field col s6">
                                <input value="<?php echo $forest['matriculeForet']; ?>" id="forestRegNumber"
                                       name="forestRegNumber" type="text" class="validate">
                                <label class="active" for="first_name2">Matricule</label>
                            </div>
                        </div>
                        <!--                        <div class="row">-->
                        <!--                            <div class="input-field col s6">-->
                        <!--                                <select>-->
                        <!--                                    <option value="--><?php //echo $forest['idDepartement'];
                        ?><!--" selected>--><?php //echo $forest['nomDepartement'];
                        ?><!--</option>-->
                        <!--                                    --><?php
                        //                                    foreach ($departments as $department){
                        //
                        ?>
                        <!--                                        <option value="-->
                        <?php //echo $department['idDepartement'];
                        ?><!--">--><?php //echo $department['nomDepartement'];
                        ?><!--</option>-->
                        <!--                                        --><?php
                        //                                    }
                        //
                        ?>
                        <!--                                </select>-->
                        <!--                                <label>Département</label>-->
                        <!--                            </div>-->
                        <!---->
                        <!--                            <div class="input-field col s6">-->
                        <!--                                <select>-->
                        <!--                                    <option value="--><?php //echo $forest['idRegion'];
                        ?><!--" selected>--><?php //echo $forest['nomRegion'];
                        ?><!--</option>-->
                        <!--                                    --><?php
                        //                                    foreach ($regions as $region){
                        //
                        ?>
                        <!--                                        <option value="--><?php //echo $region['idRegion'];
                        ?><!--">--><?php //echo $region['nomRegion'];
                        ?><!--</option>-->
                        <!--                                        --><?php
                        //                                    }
                        //
                        ?>
                        <!--                                </select>-->
                        <!--                                <label>Région</label>-->
                        <!--                            </div>-->
                        <!--                        </div>-->
                        <div class="row center-align">
                            <button class="btn waves-effect waves-light green darken-2" type="submit">Modifier
                                <i class="material-icons right">build</i>
                            </button>
                        </div>
                    </form>
                    <div class="row center-align">
                        <a href="index.php?deleteForest=<?php echo $forest['idForet']; ?>"
                           class="waves-effect waves-light red darken-2 btn"><i class="material-icons right">delete</i>Supprimer</a>
                    </div>
                </div>
            </div>
        </div>

        <?php
        require_once "view/includes/scripts.html";
        require_once "view/includes/footer.html";
        ?>

        </body>
        </html>

        <?php
    }
}
