<?php

class UpdateViews
{
    public function generateUpdateTownView($town, $epcis)
    {
        ?>
        <!DOCTYPE html>
        <html lang="fr">
        <!--        HEAD-->
        <?php require_once "view/includes/head.html" ?>

        <body>
        <?php
        require_once "view/includes/header.html";
        ?>
        <div id="mainContainer" class="white container z-depth-5">
            <div class="section">
                <h5 class="center">Consulter/Modifier</h5>
            </div>
            <div class="section">
                <?php
                //                                    var_dump($forest);
                ?>
                <div class="row">
                    <form class="col s12" method="post"
                          action="index.php?updateTown=<?php echo($town['idCommune']); ?>">
                        <div class="row">
                            <div class="input-field col s6">
                                <input value="<?php echo $town['nomCommune']; ?>" id="townName" name="townName"
                                       type="text" class="validate">
                                <label class="active" for="townName">Nom</label>
                            </div>
                            <div class="input-field col s6">
                                <input value="<?php echo $town['codeINSEE']; ?>" id="insee" name="insee" type="text"
                                       class="validate">
                                <label class="active" for="insee">Code INSEE</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <select name="epci" id="epci">
                                    <option value="<?php echo $town['idEpci']; ?>"
                                            selected><?php echo $town['nomEpci']; ?></option>
                                    <?php
                                    foreach ($epcis as $epci) {
                                        ?>
                                        <option value="<?php echo $epci['idEpci']; ?>"><?php echo $epci['nomEpci']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="epci">EPCI</label>
                            </div>
                        </div>
                        <div class="row center-align">
                            <button class="btn waves-effect waves-light green darken-2" type="submit">Modifier
                                <i class="material-icons right">build</i>
                            </button>
                        </div>
                    </form>
                    <div class="row center-align">
                        <a href="index.php?deleteTown=<?php echo $town['idCommune']; ?>"
                           class="waves-effect waves-light red darken-2 btn"><i class="material-icons right">delete</i>Supprimer</a>
                    </div>
                </div>
            </div>
        </div>

        <?php
        require_once "view/includes/scripts.html";
        require_once "view/includes/footer.html";
        ?>

        </body>
        </html>

        <?php
    }

    public function generateUpdateEpciView($epci, $departments)
    {
        ?>
        <!DOCTYPE html>
        <html lang="fr">
        <!--        HEAD-->
        <?php require_once "view/includes/head.html" ?>

        <body>
        <?php
        require_once "view/includes/header.html";
        ?>
        <div id="mainContainer" class="white container z-depth-5">
            <div class="section">
                <h5 class="center">Consulter/Modifier</h5>
            </div>
            <div class="section">
                <div class="row">
                    <form class="col s12" method="post" action="index.php?updateEpci=<?php echo($epci['idEpci']); ?>">
                        <div class="row">
                            <div class="input-field col s6">
                                <input value="<?php echo $epci['nomEpci']; ?>" id="epciName" name="epciName" type="text"
                                       class="validate">
                                <label class="active" for="townName">Nom</label>
                            </div>
                            <div class="input-field col s6">
                                <input value="<?php echo $epci['codeEPCI']; ?>" id="epciCode" name="epciCode"
                                       type="text" class="validate">
                                <label class="active" for="insee">Code EPCI</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <select name="department" id="department">
                                    <option value="<?php echo $epci['idDepartement']; ?>"
                                            selected><?php echo $epci['nomDepartement']; ?></option>
                                    <?php
                                    foreach ($departments as $department) {
                                        ?>
                                        <option value="<?php echo $department['idDepartement']; ?>"><?php echo $department['nomDepartement']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="department">Département</label>
                            </div>
                        </div>
                        <div class="row center-align">
                            <button class="btn waves-effect waves-light green darken-2" type="submit">Modifier
                                <i class="material-icons right">build</i>
                            </button>
                        </div>
                    </form>
                    <div class="row center-align">
                        <a href="index.php?deleteEpci=<?php echo $epci['idEpci']; ?>"
                           class="waves-effect waves-light red darken-2 btn"><i class="material-icons right">delete</i>Supprimer</a>
                    </div>
                </div>
            </div>
        </div>

        <?php
        require_once "view/includes/scripts.html";
        require_once "view/includes/footer.html";
        ?>

        </body>
        </html>

        <?php
    }

    public function generateUpdateDepartmentView($department, $regions)
    {
        ?>
        <!DOCTYPE html>
        <html lang="fr">
        <!--        HEAD-->
        <?php require_once "view/includes/head.html" ?>

        <body>
        <?php
        require_once "view/includes/header.html";
        ?>
        <div id="mainContainer" class="white container z-depth-5">
            <div class="section">
                <h5 class="center">Consulter/Modifier</h5>
            </div>
            <div class="section">
                <div class="row">
                    <form class="col s12" method="post"
                          action="index.php?updateDepartment=<?php echo($department['idDepartement']); ?>">
                        <div class="row">
                            <div class="input-field col s6">
                                <input value="<?php echo $department['nomDepartement']; ?>" id="departmentName"
                                       name="departmentName" type="text" class="validate">
                                <label class="active" for="departmentName">Nom</label>
                            </div>
                            <div class="input-field col s6">
                                <input value="<?php echo $department['numDepartement']; ?>" id="departmentNum"
                                       name="departmentNum" type="text" class="validate">
                                <label class="active" for="departmentNum">Numéro de département</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <select name="region" id="region">
                                    <option value="<?php echo $department['idRegion']; ?>"
                                            selected><?php echo $department['nomRegion']; ?></option>
                                    <?php
                                    foreach ($regions as $region) {
                                        ?>
                                        <option value="<?php echo $region['idRegion']; ?>"><?php echo $region['nomRegion']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="region">Région</label>
                            </div>
                        </div>
                        <div class="row center-align">
                            <button class="btn waves-effect waves-light green darken-2" type="submit">Modifier
                                <i class="material-icons right">build</i>
                            </button>
                        </div>
                    </form>
                    <div class="row center-align">
                        <a href="index.php?deleteDepartment=<?php echo $department['idDepartement']; ?>"
                           class="waves-effect waves-light red darken-2 btn"><i class="material-icons right">delete</i>Supprimer</a>
                    </div>
                </div>
            </div>
        </div>

        <?php
        require_once "view/includes/scripts.html";
        require_once "view/includes/footer.html";
        ?>

        </body>
        </html>

        <?php
    }

    public function generateUpdateRegionView($region)
    {
        ?>
        <!DOCTYPE html>
        <html lang="fr">
        <!--        HEAD-->
        <?php require_once "view/includes/head.html" ?>

        <body>
        <?php
        require_once "view/includes/header.html";
        ?>
        <div id="mainContainer" class="white container z-depth-5">
            <div class="section">
                <h5 class="center">Consulter/Modifier</h5>
            </div>
            <div class="section">
                <div class="row">
                    <form class="col s12" method="post"
                          action="index.php?updateRegion=<?php echo($region['idRegion']); ?>">
                        <div class="row">
                            <div class="input-field col s6">
                                <input value="<?php echo $region['nomRegion']; ?>" id="regionName" name="regionName"
                                       type="text" class="validate">
                                <label class="active" for="regionName">Nom</label>
                            </div>
                        </div>
                        <div class="row center-align">
                            <button class="btn waves-effect waves-light green darken-2" type="submit">Modifier
                                <i class="material-icons right">build</i>
                            </button>
                        </div>
                    </form>
                    <div class="row center-align">
                        <a href="index.php?deleteRegion=<?php echo $region['idRegion']; ?>"
                           class="waves-effect waves-light red darken-2 btn"><i class="material-icons right">delete</i>Supprimer</a>
                    </div>
                </div>
            </div>
        </div>

        <?php
        require_once "view/includes/scripts.html";
        require_once "view/includes/footer.html";
        ?>

        </body>
        </html>

        <?php
    }

}
