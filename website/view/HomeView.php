<?php


class HomeView
{
    public function generateHomeView($forests, $towns, $epcis, $departments,$regions)
    {
        ?>
        <!DOCTYPE html>
        <html lang="fr">
        <!--        HEAD-->
        <?php require_once "view/includes/head.html" ?>

        <body>
        <?php
        require_once "view/includes/header.html";
        ?>
        <div id="mainContainer" class="white container z-depth-5">
            <div class="section">
                <h5 class="center">Gestion des forêts publiques</h5>
            </div>
            <div class="section">
                <div id="mapid"></div>
                <script>
                    let mymap = L.map('mapid').setView([47.081012, 2.398782], 5);
                    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
                        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                        maxZoom: 18,
                        id: 'mapbox/streets-v11',
                        accessToken: 'pk.eyJ1IjoicmFtYTQ5IiwiYSI6ImNrNmY5bjJ4ZDBvbzAzZW52OGFjbTl0dGIifQ.7CS-y4u-cqpE-OEbuF3-Eg'
                    }).addTo(mymap);

                    // //Markers
                    let marker;

                </script>
                <?php
                foreach ($forests as $f) {
                    $gpsXY = explode(",", $f['coordonnees']);
                    ?>

                    <script>
                        //Markers
                        marker = L.marker([<?php echo $gpsXY[0]?>, <?php echo $gpsXY[1]?>]).addTo(mymap);
                        marker.bindPopup("<b><?php echo $f['nomForet']?></b><br><a href=\"index.php?forestId=<?php echo $f['idForet']?>\">Consulter/Modifier</a>").openPopup();
                    </script>
                    <?php
                }
                ?>
            </div>
            <div class="section">
<!--                TOWNS-->
                <form class="col s12" method="post" action="index.php?updateTownView=1">
                    <div class="row">
                        <div class="input-field col s6">
                            <select id="town" name="town">
                                <?php
                                foreach ($towns as $town){
                                    ?>
                                    <option value="<?php echo $town['idCommune'];?>"><?php echo $town['nomCommune'];?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <label for="town">Commune</label>
                        </div>
                        <div class="input-field col s6">
                            <button class="btn waves-effect waves-light green darken-2" type="submit">Modifier
                                <i class="material-icons right">build</i>
                            </button>
                        </div>
                    </div>
                </form>
                <form class="col s12" method="post" action="index.php?updateEpciView=1">
                    <div class="row">
                        <div class="input-field col s6">
                            <select id="epci" name="epci">
                                <?php
                                foreach ($epcis as $epci){
                                    ?>
                                    <option value="<?php echo $epci['idEpci'];?>"><?php echo $epci['nomEpci'];?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <label for="town">EPCI</label>
                        </div>
                        <div class="input-field col s6">
                            <button class="btn waves-effect waves-light green darken-2" type="submit">Modifier
                                <i class="material-icons right">build</i>
                            </button>
                        </div>
                    </div>
                </form>
                <form class="col s12" method="post" action="index.php?updateDepartmentView=1">
                    <div class="row">
                        <div class="input-field col s6">
                            <select id="department" name="department">
                                <?php
                                foreach ($departments as $department){
                                    ?>
                                    <option value="<?php echo $department['idDepartement'];?>"><?php echo $department['nomDepartement'];?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <label for="town">Département</label>
                        </div>
                        <div class="input-field col s6">
                            <button class="btn waves-effect waves-light green darken-2" type="submit">Modifier
                                <i class="material-icons right">build</i>
                            </button>
                        </div>
                    </div>
                </form>
                <form class="col s12" method="post" action="index.php?updateRegionView=1">
                    <div class="row">
                        <div class="input-field col s6">
                            <select id="region" name="region">
                                <?php
                                foreach ($regions as $region){
                                    ?>
                                    <option value="<?php echo $region['idRegion'];?>"><?php echo $region['nomRegion'];?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <label for="town">Région</label>
                        </div>
                        <div class="input-field col s6">
                            <button class="btn waves-effect waves-light green darken-2" type="submit">Modifier
                                <i class="material-icons right">build</i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <?php
        require_once "view/includes/scripts.html";
        require_once "view/includes/footer.html";
        ?>

        </body>
        </html>

        <?php
    }
}
