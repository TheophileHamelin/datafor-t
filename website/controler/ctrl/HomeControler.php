<?php
//Home controler : Management of the home page
require_once PATH_VIEW . "/HomeView.php";
require_once PATH_MODEL . "/dao/dao.php";

class HomeControler
{
    private $homeView;
    private $dao;

    //Constructor
    public function __construct(Dao $dao)
    {
        $this->dao = $dao;
        $this->homeView = new HomeView();
    }

    public function generateHomeView()
    {
        try {
            $forests = $this->dao->getForests();
            $towns = $this->dao->getTowns();
            $epcis = $this->dao->getEpcis();
            $departments = $this->dao->getDepartments();
            $regions = $this->dao->getRegions();
            $this->homeView->generateHomeView($forests, $towns, $epcis, $departments, $regions);
        } catch (DatabaseException $e) {
            echo $e->getMessage();
        }
    }
}
