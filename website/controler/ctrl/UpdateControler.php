<?php

require_once PATH_VIEW . "/HomeView.php";
require_once PATH_MODEL . "/dao/dao.php";
require_once PATH_VIEW . "/UpdateViews.php";
require_once PATH_CONTROLER . "/ctrl/HomeControler.php";

class UpdateControler
{
    private $dao;
    private $homeView;
    private $ctrlHome;
    private $updateView;

    //Constructor
    public function __construct(Dao $dao)
    {
        $this->dao = $dao;
        $this->ctrlHome = new HomeControler($dao);
        $this->homeView = new HomeView();
        $this->updateView = new UpdateViews();
    }

    public function updateTownView($townId)
    {
        $epcis = $this->dao->getEpcis();
        $town = $this->dao->getTownById($townId);
        $this->updateView->generateUpdateTownView($town, $epcis);
    }

    public function updateTown($townId, $insee, $townName, $epciId)
    {
        $this->dao->updateTown($townId, $insee, $townName, $epciId);
        $this->ctrlHome->generateHomeView();
    }

    public function updateEpciView($epciId)
    {
        $departments = $this->dao->getDepartments();
        $epci = $this->dao->getEpciById($epciId);
        $this->updateView->generateUpdateEpciView($epci, $departments);
    }

    public function updateEpci($epciId, $epciCode, $epciName, $departmentId)
    {
        $this->dao->updateEpci($epciId, $epciCode, $epciName, $departmentId);
        $this->ctrlHome->generateHomeView();
    }

    public function updateDepartmentView($departmentId)
    {
        $regions = $this->dao->getRegions();
        $department = $this->dao->getDepartmentById($departmentId);
        $this->updateView->generateUpdateDepartmentView($department, $regions);
    }

    public function updateDepartment($departmentId, $departmentName, $departmentNum, $regionId)
    {
        $this->dao->updateDepartment($departmentId, $departmentName, $departmentNum, $regionId);
        $this->ctrlHome->generateHomeView();
    }

    public function updateRegionView($regionId)
    {
        $region = $this->dao->getRegionById($regionId);
        $this->updateView->generateUpdateRegionView($region);
    }

    public function updateRegion($regionId, $regionName)
    {
        $this->dao->updateRegion($regionId, $regionName);
        $this->ctrlHome->generateHomeView();
    }
}
