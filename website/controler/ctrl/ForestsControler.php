<?php
require_once PATH_VIEW . "/HomeView.php";
require_once PATH_VIEW . "/ForestsView.php";
require_once PATH_MODEL . "/dao/dao.php";

class ForestsControler
{
    private $homeView;
    private $dao;
    private $forestsView;

    //Constructor
    public function __construct(Dao $dao)
    {
        $this->dao = $dao;
        $this->homeView = new HomeView();
        $this->forestsView = new ForestsView();
    }

    public function generateForestView($forestId)
    {
        try {
            $towns = $this->dao->getTowns();
            $epcis = $this->dao->getEpcis();
            $regions = $this->dao->getRegions();
            $departments = $this->dao->getDepartments();
            $forest = $this->dao->getForestById($forestId);
            $this->forestsView->generateForestsView($forest, $towns, $epcis, $regions, $departments);
        } catch (DatabaseException $e) {
            echo $e->getMessage();
        }
    }

    public function updateForest($forestId, $forestName, $forestRegNumber, $gps, $stateOwned, $townId)
    {
        try {
            $this->dao->updateForest($forestId, $forestName, $forestRegNumber, $gps, $stateOwned, $townId);
            $this->generateForestView($forestId);
        } catch (DatabaseException $e) {
            echo $e->getMessage();
        }
    }
}
