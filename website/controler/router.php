<?php
require_once PATH_CONTROLER . "/ctrl/HomeControler.php";
require_once PATH_CONTROLER . "/ctrl/ForestsControler.php";
require_once PATH_CONTROLER . "/ctrl/UpdateControler.php";
require_once PATH_MODEL . "/dao/dao.php";

Class Router
{

    private $dao;
    private $ctrlHome;
    private $ctrlForests;
    private $ctrlUpdate;

    //Constructor
    public function __construct()
    {
        $this->dao = new dao();
        $this->ctrlHome = new HomeControler($this->dao);
        $this->ctrlForests = new ForestsControler($this->dao);
        $this->ctrlUpdate = new UpdateControler($this->dao);
    }

    public function conductRequest()
    {
        //CONSULTING/EDITING A FOREST
        if(isset($_GET['forestId'])){
            $this->ctrlForests->generateForestView($_GET['forestId']);
        }else if(isset($_GET['updateForest'])){
            //UPDATING FOREST
            if(isset($_POST['forestRegNumber']) && !empty($_POST['forestRegNumber']) && isset($_POST['forestName']) && !empty($_POST['forestName']) && isset($_POST['gps']) && !empty($_POST['gps']) && isset($_POST['stateOwned']) && isset($_POST['town']) && !empty($_POST['town'])){
                $this->ctrlForests->updateForest($_GET['updateForest'], $_POST['forestName'], $_POST['forestRegNumber'], $_POST['gps'], $_POST['stateOwned'], $_POST['town']);
            }else{
                $this->ctrlForests->generateForestView($_GET['updateForest']);
            }
        }else if(isset($_GET['updateTownView'])){
            //UPDATING TOWN VIEW
            if(isset($_POST['town']) && !empty($_POST['town'])){
                $this->ctrlUpdate->updateTownView($_POST['town']);
            }else{
                $this->ctrlHome->generateHomeView();
            }
        }else if(isset($_GET['updateTown'])){
            //UPDATING TOWN
            if(isset($_POST['townName']) && !empty($_POST['townName']) && isset($_POST['insee']) && !empty($_POST['insee']) && isset($_POST['epci']) && !empty($_POST['epci'])){
                $this->ctrlUpdate->updateTown($_GET['updateTown'], $_POST['insee'], $_POST['townName'],$_POST['epci']);
            }else{
                $this->ctrlHome->generateHomeView();
            }
        }else if(isset($_GET['updateEpciView'])){
            //UPDATING EPCI VIEW
            if(isset($_POST['epci']) && !empty($_POST['epci'])){
                $this->ctrlUpdate->updateEpciView($_POST['epci']);
            }else{
                $this->ctrlHome->generateHomeView();
            }
        }else if(isset($_GET['updateEpci'])){
            //UPDATING EPCI
            if(isset($_POST['epciName']) && !empty($_POST['epciName']) && isset($_POST['epciCode']) && !empty($_POST['epciCode']) && isset($_POST['department']) && !empty($_POST['department'])){
                $this->ctrlUpdate->updateEpci($_GET['updateEpci'], $_POST['epciCode'], $_POST['epciName'],$_POST['department']);
            }else{
                $this->ctrlHome->generateHomeView();
            }
        }else if(isset($_GET['updateDepartmentView'])){
            //UPDATING DEPARTMENT VIEW
            if(isset($_POST['department']) && !empty($_POST['department'])){
                $this->ctrlUpdate->updateDepartmentView($_POST['department']);
            }else{
                $this->ctrlHome->generateHomeView();
            }
        }else if(isset($_GET['updateDepartment'])){
            //UPDATING DEPARTMENT
            if(isset($_POST['departmentName']) && !empty($_POST['departmentName']) && isset($_POST['departmentNum']) && !empty($_POST['departmentNum']) && isset($_POST['region']) && !empty($_POST['region'])){
                $this->ctrlUpdate->updateDepartment($_GET['updateDepartment'], $_POST['departmentName'], $_POST['departmentNum'],$_POST['region']);
            }else{
                $this->ctrlHome->generateHomeView();
            }
        }else if(isset($_GET['updateRegionView'])){
            //UPDATING DEPARTMENT VIEW
            if(isset($_POST['region']) && !empty($_POST['region'])){
                $this->ctrlUpdate->updateRegionView($_POST['region']);
            }else{
                $this->ctrlHome->generateHomeView();
            }
        }else if(isset($_GET['updateRegion'])){
            //UPDATING DEPARTMENT
            if(isset($_POST['regionName']) && !empty($_POST['regionName'])){
                $this->ctrlUpdate->updateRegion($_GET['updateRegion'], $_POST['regionName']);
            }else{
                $this->ctrlHome->generateHomeView();
            }
        }else if(isset($_GET['deleteForest'])){
            //DELETING FOREST
            $this->dao->deleteForest($_GET['deleteForest']);
            $this->ctrlHome->generateHomeView();
        }else if(isset($_GET['deleteTown'])){
            //DELETING FOREST
            $this->dao->deleteTown($_GET['deleteTown']);
            $this->ctrlHome->generateHomeView();
        }else if(isset($_GET['deleteEpci'])){
            //DELETING FOREST
            $this->dao->deleteEpci($_GET['deleteEpci']);
            $this->ctrlHome->generateHomeView();
        }else if(isset($_GET['deleteDepartment'])){
            //DELETING FOREST
            $this->dao->deleteDepartment($_GET['deleteDepartment']);
            $this->ctrlHome->generateHomeView();
        }else if(isset($_GET['deleteRegion'])){
            //DELETING FOREST
            $this->dao->deleteRegion($_GET['deleteRegion']);
            $this->ctrlHome->generateHomeView();
        }else{
            //DEFAULT : HOME PAGE
            $this->ctrlHome->generateHomeView();
        }




    }
}
